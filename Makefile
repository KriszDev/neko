#arch for kernel arch/ARCH/*
ARCH := x86_64
#arch for qemu ARCH-system-qemu
QARCH := x86_64
#gcc cross compiler arch ARCH-gcc
GCC_ARCH := x86_64-elf

#define qemu additional flags for all targets
#QEMU_HARDWARE := -serial /dev/ttyS16
QEMU_HARDWARE :=

#c compiler name
CC := $(GCC_ARCH)-gcc
#c++ compiler name
CXX := $(GCC_ARCH)-g++
#gas syntax assembler
ASS := $(GCC_ARCH)-as

INCLUDES := -Iinclude
CXXFLAGS := -mno-red-zone -mcmodel=kernel -ffreestanding -Wall -Wextra -fno-builtin -MMD -MP -nostdlib -fno-exceptions -fno-rtti -O2 -g $(INCLUDES) # -Wl,-Map,kernel.map
CRTBEGIN := $(shell $(CXX) $(CXXFLAGS) -print-file-name=crtbegin.o)
CRTEND := $(shell $(CXX) $(CXXFLAGS) -print-file-name=crtend.o)

OBJ         = $(patsubst %.cpp,%.o,$(wildcard kernel/*.cpp))
OBJ        += $(patsubst %.cpp,%.o,$(wildcard kernel/*/*.cpp))

OBJ        += $(patsubst %.cpp,%.o,$(wildcard arch/${ARCH}/*.cpp))
OBJ        += $(patsubst %.cpp,%.o,$(wildcard arch/${ARCH}/*/*.cpp))

OBJ        += $(patsubst %.cpp,%.o,$(wildcard lib/*.cpp))
OBJ        += $(patsubst %.cpp,%.o,$(wildcard lib/*/*.cpp))

OBJ_ASM     = $(patsubst %.S,%.o,$(wildcard arch/${ARCH}/*.S))

ASM_OTHERS  = $(patsubst %.asm,%.o,$(wildcard arch/${ARCH}/*.asm))

KERNEL_SRC  = $(wildcard kernel/*.cpp) $(wildcard kernel/*/*.cpp)
KERNEL_SRC += $(wildcard arch/${ARCH}/*.cpp) $(wildcard arch/${ARCH}/*/*.cpp)
KERNEL_SRC += $(wildcard arch/${ARCH}/*.S) $(wildcard arch/${ARCH}/*/*.S)

#additional objects to link before and after other files (excluded from compilation before before)
OBJ_PRE   := arch/$(ARCH)/crti.o $(CRTBEGIN)
OBJ_AFTER := $(CRTEND) arch/$(ARCH)/crtn.o

qemu-run: out/boot.iso
	qemu-system-$(QARCH) $(QEMU_HARDWARE) -m 128M -cdrom out/boot.iso

int-debug:
	qemu-system-$(QARCH) $(QEMU_HARDWARE) -m 128M -cdrom out/boot.iso -d int

debug-run: out/boot.iso
	qemu-system-$(QARCH) $(QEMU_HARDWARE) -m 128M -cdrom out/boot.iso -d int -s -S

full-debug-run: out/boot.iso
	qemu-system-$(QARCH) $(QEMU_HARDWARE) -m 128M -cdrom out/boot.iso -d in_asm,cpu_reset,exec,int,guest_errors,pcall -D full_debug.log

#boot iso image (qemu can boot without iso though)
out/boot.iso: isodir/boot/myos.bin
	@mkdir -p out
	grub-mkrescue -o out/boot.iso isodir/

#the kernel + kernel libs
isodir/boot/myos.bin: $(OBJ_PRE) $(OBJ) $(OBJ_ASM) $(OBJ_AFTER)
	$(CXX) -T arch/$(ARCH)/linker.ld $(CXXFLAGS) -lgcc -o $@ $^
	objcopy --only-keep-debug $@ symbols.sym
	objcopy --strip-debug $@

#for each .cpp file
%.o: %.cpp %.S %.asm
	$(CXX) $(CXXFLAGS) -c $^ -o $@

#for each normal asm file (that doesn't matter when it's linked)
%.o: %.S
	$(CXX) $(CXXFLAGS) -c $^ -o $@

#when it matters when it's done
%.o: %.asm
	$(ASS) $< -o $@

#remove all files made by compilation
clean:
	-find -name *.o -delete
	-find -name *.d -delete
	-rm isodir/boot/myos.bin
	-rm symbols.sym

#stub to compiler other stuff
others: $(ASM_OTHERS)
	-echo ok
